:: discasset quick runner

:: download the latest data
node download
:: extract css assets
node extract-css -skip
:: extract assets from cdn out of data.js.txt
node extract-cdn-assets -skip
:: extract assets from the other main client file
node extract-cdn2 -skip
:: extract svg icons from data.js for format 1
node extract-svg -skip
:: extract svg icons from data.js for format 2
node extract-svg2 -skip
:: extract lotties from webpack modules
node extract-lottie